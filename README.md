# Vyhledání podřetězce v řetězci
Na vstup je dán řetězec a podřetězec, algoritmus najde index prvního výskytu podřetězce v textu. Pokud pattern není 
nalezen, program vrácí "Pattern was not found in a string", pokud řetezec obsahuje více výskytů podřetězce, program
vrácí je všechny.
Tento problém v dané semestrální práce byl vyřešen pomoci dvou algoritmů - Knuth-Morris-Pratt (KMP) a Boyer–Moore. 

## Implementace
Interaktivně lze výbrat prefirovaný algoritmus - KMP nebo Boyer-Moore. Mezery v řetězci nejsou povolené. Pokud chcete 
otestovat program testovými data ze souboru, je potřeba uvést Absolute path souboru ve filename.

### Boyer-Moore řešení:
Pro implementaci algoritmu byly definovany dvě funkce - `bad_char_proc` a `bm_search`. 
Funkce `bad_char_proc` je pomocná funkce, která vytváří tabulku "bad character shift", která je podstatná pro
algoritmus Boyer-Moore.
CHAR_RANGE je nastaven na 256, což reprezentuje celý rozsah ASCII. 

Funkce `bm_search` je hlavní hledací funkce. Před začátkem hledání se vytvoří tabulka "bad character shift", která 
určuje, na kolik znaků se má pattern posunout při neshodě.
Algoritmus postupně posouvá pattern po textu a porovnává jej. Pokud se vzor shoduje s částí textu, funkce vypíše 
pozici shody.
Pokud dojde k neshodě, algoritmus využívá tabulku  "bad character shift" pro výpočet, o kolik znaků se má pattern 
posunout dál.
Pokud se nenalezne žádná shoda, funkce vrací "Pattern was not found in a text".
Časová složitost algoritmu je O(n/m), kde n je délka textu a m délka patternu pro vyhledavaní.

### Knuth–Morris–Pratt
Pro implementaci algoritmu byly definovany dvě funkce `LPS` a `kmp_search`.
Funkce `LPS` je pomocná funkce pro vytvoření pole Nejdelšího Prefixu, který je zároveň Sufixem, pro daný pattern.

Funkce `kmp_search` je hlavní hledací funkce. Pomocí funkce `LPS` se vytvoří pole LPS (Longest Prefix Suffix), 
které je klíčové pro algoritmus KMP.
Pokud pattern[j] (znak v patternu) se shoduje s aktualnim znakem v textu text[i], oba indexy se podouvají o jeden dopředu.
Pokud index patternu j se rovná delce patternu, vypiše se pozice shody patternu a textu. 
Pokud se znaky neshodují, j se aktualizuje podle hodnoty v poli LPS.
Časová složitost algoritmu je O(n + m), kde kde n je délka řetězce a m je délka patternu pro vyhledavání.
Na konci programu se vypiše čas, za který algoritmus zpracoval řetězec. 


## Měření
Měření proběhlo vůči kódu v commitu 0f5eab384eb18d22995898395f7b23bf965b56ef na procesoru Intel® Core™ i5-8265U CPU @ 1.60GHz × 8. OS: Ubuntu 22.04.2 LTS.
Testováno na 7 souborech, které obsahují různý počet symbolů, ketré skladají slova například "apple", "mysterious",
"adventure", "python", "ocean", "galaxy", "music", atd. Hledaný pattern - energy. Pattern vyskytuje v
textu vícekrát.

soubor string5551.txt - 5551 symbolů
soubor string27993.txt - 27993 symbolu
soubor string60042.txt - 60042 symbolu
soubor string112388.txt - 112388 symbolu
soubor string281099.txt - 281099 symbolu
soubor string562627.txt - 562627 symbolu
soubor string2814963.txt - 2814963 symbolu

Po testování algoritmů výsledky byly nasledujicí:

Algoritmus BM soubor string5551.txt Hledaný pattern - energy :Needed 0 ms to finish.

Algoritmus KMP soubor string5551.txt Hledaný pattern - energy :Needed 0 ms to finish.


Algoritmus BM soubor string27993.txt Hledaný pattern - energy : Needed 1 ms to finish.

Algoritmus KMP soubor string27993.txt Hledaný pattern - energy : Needed 2 ms to finish.


Algoritmus BM soubor string60042.txt Hledaný pattern - energy : Needed 6 ms to finish.

Algoritmus KMP soubor string60042.txt Hledaný pattern - energy : Needed 6 ms to finish.


Algoritmus BM soubor string112388.txt Hledaný pattern - energy :Needed 9 ms to finish.

Algoritmus KMP soubor string112388.txt Hledaný pattern - energy : Needed 11 ms to finish.


Algoritmus BM soubor string281099.txt Hledaný pattern - energy :Needed 100 ms to finish.

Algoritmus KMP soubor string281099.txt Hledaný pattern - energy :Needed 90 ms to finish.


Algoritmus BM soubor string562627.txt Hledaný pattern - energy :Needed 200 ms to finish.

Algoritmus KMP soubor string562627.txt Hledaný pattern - energy :Needed 190 ms to finish.


Algoritmus BM soubor string2814963.txt Hledaný pattern - energy :Needed 500 ms to finish.

Algoritmus KMP soubor string2814963.txt Hledaný pattern - energy :Needed 510 ms to finish.

Vysledky jsou znazorneny na nasledujicim grafu pro porovnani:

![img.png](img.png)


Na obrázku je vidět, že čas za jaký algoritmy našli pattern "energy" byl skoro stejný. Jenom ze souboru s 
112388 symboly a až do souboru s 562627 symboly algoritmus KMP search byl o trochu rychlejší.
