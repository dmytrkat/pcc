#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include <vector>
#include <cstring>

using namespace std;

void bm_search(const std::string &text, const std::string &pattern);

void kmp_search(const std::string &text, const std::string &pattern);

template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void printHelp() {
    std::cout << "How to use a program:\n";

    std::cout << "For interactive way:\n";
    std::cout << "./semestralka -inter\n";
    std::cout << "----------------------------------------------------------------------\n";
    std::cout << "For non-interactive way Knuth-MorrisPrattSearch reading from a string :\n";
    std::cout << "./semestralka -s1 string -s2 pattern -kmp\n";
    std::cout << "----------------------------------------------------------------------\n";
    std::cout << "For non-interactive way Boyer-Moore Search reading from a string :\n";
    std::cout << "./semastralka -s1 string -s2 pattern -bm\n";
    std::cout << "----------------------------------------------------------------------\n";
    std::cout << "For non-interactive way Knuth-MorrisPrattSearch reading from a file :\n";
    std::cout << "Example pattern: energy\n";
    std::cout << "./semestralka -f filename -s2 pattern -kmp\n";
    std::cout << "----------------------------------------------------------------------\n";
    std::cout << "For non-interactive way Boyer Moore Search reading from a file :\n";
    std::cout << "Example pattern: energy\n";
    std::cout << "./semestralka -f filename -s2 pattern -bm\n";
    std::cout << "----------------------------------------------------------------------\n";
    std::cout << "inter : Read input from standard input\n";
}

std::string read_input_from_file(const std::string &source) {
    std::string data;
    std::ifstream file(source);
    if (file.is_open()) {
        std::getline(file, data);
        file.close();
    } else {
        std::cerr << "Error occurred while opening file: " << source << "\n";
        exit(1);
    }
    return data;
}

bool are_spaces(const std::string &string) {
    if (string.find_first_of(' ') != std::string::npos) {
        return true;
    } else {
        return false;
    }
}

bool is_valid_input_type(std::string &input_type, std::string &text, std::string &pattern, std::string &input_filename) {
    if (input_type == "string_from_input") {
        if (are_spaces(text)) {
            std::cerr << "String should not contain any spaces!\n";
            return false;
        }
        if (are_spaces(pattern)) {
            std::cerr << "Pattern should not contain any spaces!\n";
            return false;
        }
    }
    if (input_type == "file") {
        const std::string &filename = input_filename;
        text = read_input_from_file(filename);
        if (are_spaces(text)) {
            std::cerr << "String from a file should not contain any spaces!\n";
            return false;
        }
    }
    return true;
}

int main(int argc, char **argv) {
    if (argc == 1 || strcmp(argv[1], "--h") == 0 || strcmp(argv[1], "--help") == 0) {
        printHelp();
        return 0;
    }

    std::string text;
    std::string pattern;
    std::string input_type;
    std::string algorithm;
    std::string input_filename;

    if (argc == 6) {
        if (strcmp(argv[1], "-s1") == 0) {
            text = argv[2];
            input_type = "string_from_input";
        } else if (strcmp(argv[1], "-f") == 0) {
            input_filename = argv[2];
            input_type = "file";
        } else {
            std::cerr << "This argument must be -s1 or -f! Your input is: " << argv[1] << '\n';
            printHelp();
            return 1;
        }

        if (strcmp(argv[3], "-s2") == 0) {
            pattern = argv[4];
        } else {
            std::cerr << "This argument must be -s2! Your input is: " << argv[3] << '\n';
            printHelp();
            return 1;
        }

        if (strcmp(argv[5], "-kmp") == 0) {
            algorithm = "kmp";
        } else if (strcmp(argv[5], "-bm") == 0) {
            algorithm = "bm";
        } else{
            std::cerr << "This argument must be -kmp or -bm! Your input is: " << argv[5] << '\n';
            printHelp();
            return 1;
        }

    } else if (argc == 2) {
        if (strcmp(argv[1], "-inter") == 0) {
            input_type = "string_from_input";
            std::cout << "Enter string : ";
            std::getline(std::cin, text);
            std::cout << "Enter pattern: ";
            std::getline(std::cin, pattern);
            std::cout << "Enter desired algorithm : ";
            std::getline(std::cin, algorithm);
        } else {
            std::cerr << "This argument must be -inter! Your input is: " << argv[1] << '\n';
            printHelp();
            return 1;
        }
    }

    if (is_valid_input_type(input_type, text, pattern, input_filename) && (argc == 2 || argc == 6)) {
        if (algorithm == "bm") {
            bm_search(text, pattern);
        } else if (algorithm == "kmp") {
            kmp_search(text, pattern);
        } else{
            std::cerr << "Algorithm should be -kmp or -bm only\n";
        }
    } else{
        std::cerr << "Your input has an error\n";
        return 1;
    }
}


const int CHAR_RANGE = 256;
void bad_char_proc(const std::string &pat, int BCS[CHAR_RANGE]) {
    for (int i = 0; i < CHAR_RANGE; ++i) {
        BCS[i] = -1;
    }
    for (size_t i = 0; i < pat.size(); ++i) {
        BCS[static_cast<int>(pat[i])] = i;
    }
}


void bm_search(const std::string &text, const std::string &pattern) {
    int p_len = pattern.size();
    int t_len = text.size();
    int bcs[CHAR_RANGE];

    bool pattern_found = false;

    auto start = std::chrono::high_resolution_clock::now();
    bad_char_proc(pattern, bcs);

    int shift = 0;
    while (shift <= (t_len - p_len)) {
        int i = p_len - 1;

        while (i >= 0 && pattern[i] == text[shift + i]) {
            --i;
        }
        if (i < 0) {
            std::cout << "Pattern found at " << shift << " index"<<std::endl;
            pattern_found = true;
            if (shift + p_len >= t_len) {
                shift += 1;
            } else {
                int bci = text[shift + p_len];
                shift += p_len - bcs[bci];
            }
        } else {
            int bci = text[shift + i];
            shift += std::max(1, i - bcs[bci]);
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    if (!pattern_found) {
        std::cout << "Pattern was not found in a string" << std::endl;
    }
    std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";

}


std::vector<int> LPS(const std::string &pattern) {
    int p_len = pattern.size();
    std::vector<int> result(p_len, 0);
    int j = 0;
    result[0] = 0;
    for (int i = 1; i < p_len;) {
        if (pattern[i] == pattern[j]) {
            j++;
            result[i] = j;
            i++;
        } else {
            if (j != 0) {
                j = result[j - 1];
            } else {
                result[i] = 0;
                i++;
            }
        }
    }
    return result;
}


void kmp_search(const std::string &text, const std::string &pattern) {
    auto start = std::chrono::high_resolution_clock::now();
    std::vector<int> lps = LPS(pattern);
    int t_len = text.length();
    int p_len = pattern.length();
    bool pattern_found = false;

    for (int i = 0, j = 0; i < t_len;) {
        if (pattern[j] == text[i]) {
            j++;
            i++;
        }

        if (j == p_len) {
            std::cout << "Pattern found at " << (i - j) <<" index" << std::endl;
            pattern_found = true;
            j = lps[j - 1];
        } else if (i < t_len && pattern[j] != text[i]) {
            if (j != 0) {
                j = lps[j - 1];
            } else {
                i++;
            }
        }
    }
    auto end = std::chrono::high_resolution_clock::now();
    if (!pattern_found) {
        std::cout << "Pattern was not found in a string" << std::endl;
    }
    std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";

}